# Goldenrod Filter Wrench

3D printable wrench for Goldenrod fuel filter housing bowls. I printed in PLA with 4 walls and 60% infill. If you find that it fits too tightly, scale it up a hair. 